//
//  ViewController.swift
//  aerisweather
//
//  Copyright (c) 2014 iAchieved.it LLC. All Rights Reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var currentLocationLabel: UILabel!
  @IBOutlet weak var currentWeatherIcon: UIImageView!
  @IBOutlet weak var currentTemperatureLabel: UILabel!
  @IBOutlet weak var currentConditionsLabel: UILabel!
  @IBOutlet weak var lastUpdatedLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(animated: Bool) {
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: "locationAvailable:",
      name: "LOCATION_AVAILABLE",
      object: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func locationAvailable(notification:NSNotification) -> Void {
    println("locationAvailable:")
    
    // Configure interface objects here.
    let aerisConsumerId =     valueForAPIKey(keyname: "AERIS_CLIENT_ID")
    let aerisConsumerSecret = valueForAPIKey(keyname: "AERIS_APP_SECRET")
    
    AerisEngine.engineWithKey(aerisConsumerId, secret: aerisConsumerSecret)

    
    let dict = notification.userInfo as Dictionary<String,AnyObject>
    
    let city          = dict["city"]! as String
    let state         = dict["state"]! as String
    let country       = dict["country"]! as String
    let lastUpdatedAt = dict["timestamp"] as NSDate
    
    
    let place = AWFPlace(city: city, state:state, country:country)
    let loader = AWFObservationsLoader()
    
    loader.getObservationForPlace(place, options: nil, completion: { (observations, error) -> Void in
      
      if observations.count > 0 {
        
        let observation = observations[0] as AWFObservation
        
        self.currentWeatherIcon.image = UIImage(named:observation.icon)
        self.currentConditionsLabel.text = observation.weather
        self.currentLocationLabel.text = "\(city), \(state)"
        self.currentTemperatureLabel.text = "\(observation.tempF)°"
        
        
        let dateFormatter = NSDateFormatter()
        let formattedDate = NSDateFormatter.localizedStringFromDate(lastUpdatedAt, dateStyle: .MediumStyle,
          timeStyle: .MediumStyle)
        self.lastUpdatedLabel.text = formattedDate
        
      } else {
        println("No observations")
        if let e = error {
          println("error:  \(e)")
        }
      }
      }
    )
}



}

