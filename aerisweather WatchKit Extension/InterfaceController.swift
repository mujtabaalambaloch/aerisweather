//
//  InterfaceController.swift
//  aerisweather WatchKit Extension
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

  @IBOutlet weak var currentLocationLabel: WKInterfaceLabel!
  @IBOutlet weak var currentWeatherIcon: WKInterfaceImage!
  @IBOutlet weak var currentTemperatureLabel: WKInterfaceLabel!
  @IBOutlet weak var currentConditionsLabel: WKInterfaceLabel!
  @IBOutlet weak var lastUpdatedLabel: WKInterfaceLabel!
  
  
    override init(context: AnyObject?) {
        // Initialize variables here.
        super.init(context: context)
        
        // Configure interface objects here.
        NSLog("%@ init", self)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
      
      let url = NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.aerisweather")
      let file = url!.URLByAppendingPathComponent("aerisweather.plist")
      
      if NSFileManager.defaultManager().fileExistsAtPath(file.path!) {
        
        let dict = NSDictionary(contentsOfURL: file) as Dictionary<String,AnyObject>
        let city          = dict["city"]! as String
        let state         = dict["state"]! as String
        let country       = dict["country"]! as String
        let lastUpdatedAt = dict["timestamp"]! as NSDate
        
        println("Last location:  \(city), \(state), at \(lastUpdatedAt)")
        
        /* Initialize Aeris */
        let aerisConsumerId =     valueForAPIKey(keyname: "AERIS_CLIENT_ID")
        let aerisConsumerSecret = valueForAPIKey(keyname: "AERIS_EXTAPP_SECRET")
        AerisEngine.engineWithKey(aerisConsumerId, secret: aerisConsumerSecret)
        
        /* Use Aeris to obtain current weather for last known location */
        let place = AWFPlace(city: city, state:state, country:country)
        let loader = AWFObservationsLoader()
        
        loader.getObservationForPlace(place, options: nil, completion: { (observations, error) -> Void in
          
          if observations.count > 0 {
            
            let observation = observations[0] as AWFObservation
            
            self.currentWeatherIcon.setImageNamed(observation.icon)
            self.currentConditionsLabel.setText(observation.weather)
            self.currentLocationLabel.setText("\(city), \(state)")
            self.currentTemperatureLabel.setText("\(observation.tempF)°")
            
            let dateFormatter = NSDateFormatter()
            let formattedDate = NSDateFormatter.localizedStringFromDate(lastUpdatedAt, dateStyle: .ShortStyle,
              timeStyle: .ShortStyle)
            self.lastUpdatedLabel.setText(formattedDate)
            
          } else {
            println("No observations")
            if let e = error {
              println("error:  \(e)")
            }
          }
        })

      } else {
        println("No last location")
      }
      
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }

}
